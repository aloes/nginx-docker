#!/bin/bash

# todo : chmod of dependencies if needed
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/helpers/build.sh"
. "$DIR/helpers/create.sh"
. "$DIR/helpers/delete.sh"
. "$DIR/helpers/env-parser.sh"
. "$DIR/helpers/log.sh"
. "$DIR/helpers/start.sh"
. "$DIR/helpers/stop.sh"
. "$DIR/helpers/letsencrypt-init.sh"

usage() {
	echo "Usage $0 -c command_name -e env_name
Commands : 
	- create
	- build
	- start
	- stop
	- log"
}

get_env_name() {
  if [ "$1" == "production" ]; then
    ENV="production"
  elif [ "$1" == "local" ]; then
    ENV="local"
  else
    ENV="local"
  fi
  echo ${ENV}
}

is_valid_command() {
  case "$1" in
	  'create')
  		return 0
	    ;;
	  'build')
  		return 0
	    ;;
	  'start')
  		return 0
	    ;;
		'stop')
  		return 0
	    ;;
	  'log')
  		return 0
	    ;; 
  esac
  return 1
}

# echo  LOGO NAME
# COPYRIGHTS LICENSE
# DESCRIPTION

# if no argument passed, prompt user to get command and environment
if [ -z "$1" ]; then
	echo "Type the command you want to execute (build|start|stop|log), followed by [ENTER]:"
	read CMD
	if is_valid_command "$CMD"; then
    echo "Type the environment used to execute $CMD (local|production), followed by [ENTER]:"
		read ENV
	else
	  usage
	  exit 1
	fi
fi

# else parse arguments from CLI
while [[ $# > 1 ]]
do
key="$1"

case $key in
  -c|--command)
      CMD="$2"
      if ! is_valid_command "$CMD"; then 
      	usage
      	exit 1
      else
      	shift
      fi
      ;;
  -e|--env)
      ENV="$2"
      shift
      ;;
  -s|--service)
      SERVICE="$2"
      shift
      ;;
  *)
  	usage
    exit 1
  	;;
esac
shift # past argument or value
done

ENV=$(get_env_name ${ENV})
SECONDS=0

if [ "$CMD" == "build" ]; then
	echo "$CMD containers for $ENV environment"
	build $ENV $SERVICE
elif [ "$CMD" == "start" ]; then
	echo "$CMD $ENV containers"
	start $ENV $SERVICE
elif [ "$CMD" == "stop" ]; then
	echo "$CMD $ENV containers"
	stop $ENV $SERVICE
elif [ "$CMD" == "log" ]; then
	echo "$CMD $ENV services"
	log $ENV $SERVICE
elif [ "$CMD" == "create" ]; then
	create $ENV
    # read -p "Would you like to build your project now ? (y/N) " answer
elif [ "$CMD" == "delete" ]; then
	echo "$CMD containers for $ENV environment"
	delete $ENV
else
	echo "Invalid command"
	usage
	exit 1
fi

