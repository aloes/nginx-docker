# Copyright 2019 Edouard Maleix, read LICENSE

worker_processes 8;

events { 
  worker_connections 2048; 
}

http {

  map $http_upgrade $connection_upgrade {
    default upgrade;
    '' close;
  }

  map $status $loggable {
    ~^[23]  0;
    default 1;
  }

  map $remote_addr $proxy_forwarded_elem {
    # IPv4 addresses can be sent as-is
    ~^[0-9.]+$          "for=$remote_addr";

    # IPv6 addresses need to be bracketed and quoted
    ~^[0-9A-Fa-f:.]+$   "for=\"[$remote_addr]\"";

    # Unix domain socket names cannot be represented in RFC 7239 syntax
    default             "for=unknown";
  }
    
  map $http_forwarded $proxy_add_forwarded {
    # If the incoming Forwarded header is syntactically valid, append to it
    "~^(,[ \\t]*)*([!#$%&'*+.^_`|~0-9A-Za-z-]+=([!#$%&'*+.^_`|~0-9A-Za-z-]+|\"([\\t \\x21\\x23-\\x5B\\x5D-\\x7E\\x80-\\xFF]|\\\\[\\t \\x21-\\x7E\\x80-\\xFF])*\"))?(;([!#$%&'*+.^_`|~0-9A-Za-z-]+=([!#$%&'*+.^_`|~0-9A-Za-z-]+|\"([\\t \\x21\\x23-\\x5B\\x5D-\\x7E\\x80-\\xFF]|\\\\[\\t \\x21-\\x7E\\x80-\\xFF])*\"))?)*([ \\t]*,([ \\t]*([!#$%&'*+.^_`|~0-9A-Za-z-]+=([!#$%&'*+.^_`|~0-9A-Za-z-]+|\"([\\t \\x21\\x23-\\x5B\\x5D-\\x7E\\x80-\\xFF]|\\\\[\\t \\x21-\\x7E\\x80-\\xFF])*\"))?(;([!#$%&'*+.^_`|~0-9A-Za-z-]+=([!#$%&'*+.^_`|~0-9A-Za-z-]+|\"([\\t \\x21\\x23-\\x5B\\x5D-\\x7E\\x80-\\xFF]|\\\\[\\t \\x21-\\x7E\\x80-\\xFF])*\"))?)*)?)*$" "$http_forwarded, $proxy_forwarded_elem";

    # Otherwise, replace it
    default "$proxy_forwarded_elem";
  }

  upstream http_api {
    server 127.0.0.1:${HTTP_SERVER_PORT};
  }

  upstream ws_api {
    server 127.0.0.1:${WS_BROKER_PORT};
  }

  log_format custom_combined '$server_addr - $remote_addr [$time_local] "$request" $status $body_bytes_sent $upstream_addr "$http_referer" "$http_user_agent"';

  server {
    listen ${PROXY_HTTP_SERVER_PORT};
    listen [::]:${PROXY_HTTP_SERVER_PORT} ipv6only=on;
    server_name ${PROXY_SERVER_HOST};

    access_log /var/log/nginx/http-gw-access.log custom_combined buffer=32k flush=1m;
    error_log /var/log/nginx/http-gw-error.log warn;

    location /app/ {
      proxy_redirect off;
      proxy_http_version 1.1;
      proxy_read_timeout 90;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Forwarded $proxy_add_forwarded;
      if ($request_uri ~* "/app(/.*$)") {
        set  $path_remainder  $1;
      }
      proxy_pass http://http_api$path_remainder;
      # proxy_pass http://http_api;
    }  

    location /ws {
      proxy_pass http://ws_api;
      proxy_http_version 1.1;
      proxy_read_timeout 90;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection $connection_upgrade;
      proxy_redirect off;

      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Forwarded $proxy_add_forwarded;
    }  
	}
}

stream {
  log_format mqtt '$server_addr - $remote_addr [$time_local] $protocol $status $bytes_received ' 
                  '$bytes_sent $upstream_addr';

  access_log /var/log/nginx/mqtt-gw-access.log mqtt buffer=32k flush=1m;
  error_log /var/log/nginx/mqtt-gw-error.log; # Health check notifications

  upstream mqtt_api {
    server 127.0.0.1:${MQTT_BROKER_PORT};
  }

  server {
    listen ${PROXY_MQTT_BROKER_PORT};
    listen [::]:${PROXY_MQTT_BROKER_PORT} ipv6only=on;

    proxy_pass mqtt_api;
    proxy_protocol on;

    # proxy_next_upstream_timeout 1;
    # proxy_next_upstream_tries 5;
    proxy_connect_timeout 5s;
    proxy_timeout 60s;
    # proxy_socket_keepalive on;
  }

}