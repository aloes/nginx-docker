#!/bin/bash

log() {
  if ! [ -x "$(command -v docker-compose)" ]; then
    echo 'Error: docker-compose is not installed.' >&2
    exit 1
  fi
  if [ -z "$1" ]; then
    echo "Environment name is required"
    exit 1
  fi

  local ENV=$1
  local SERVICE=$2
  local config="$(pwd)/.env"
  if [ ! -f "$config" ]; then
    echo "No environment file found, start by creating one"
    exit 1
  fi

  # get servicename from $2
  if [ "$SERVICE" == "proxy" ]; then
    if [ "$ENV" == "production" ]; then
      docker logs aloes-gw-prod --follow --tail="100"
    elif [ "$ENV" == "staging" ]; then
      docker logs aloes-gw-stage --follow --tail="100"
    else
      docker logs aloes-gw --follow --tail="100"
    fi
  else
    if [ "$ENV" == "production" ]; then
      docker logs aloes-gw-prod --follow --tail="100"
    elif [ "$ENV" == "staging" ]; then
      docker logs aloes-gw-stage --follow --tail="100"
    else
      docker logs aloes-gw --follow --tail="100"
    fi
  fi


}
