#!/bin/bash

get_value_from_user() {
	if [ -z "$1" ]; then
    echo "Environment variable name is required"
    exit 1
  fi

  local current_key=$1
  local env_file=$2
  local from_env=$3

  local variable_default=""
  if [ "$from_env" == "1" ]; then 
    if [ -z "$env_file" ]; then
      env_file="$(pwd).env"
    fi
    if [ ! -f "$env_file" ]; then
      echo "Env file $env_file does not exit"
      exit 1
    fi
    local key_exists=$(check_env $current_key $env_file)
    if  [ $key_exists != "0" ]; then
      variable_default=$(read_var $current_key $env_file)
    fi
    # grep -q -i "$current_key=" "$env_file"
    # if  [ $? == 0 ]; then
    #   variable_default=$(read_var $current_key $env_file)
    # fi
  fi
  if [ -z "$variable_default" ]; then
    variable_default=$(read_var $current_key .env.sample)
  fi

  # local variable_default=$(read_var $current_key .env.sample)
	echo -n "$current_key (default=$variable_default) :"
	read USER_VAL
	if [ -z "$USER_VAL" ]; then 
		USER_VAL=$variable_default 
	fi

  if [ "$from_env" == "1" ]; then
    del_env $current_key $env_file
  fi
}

replicate_env() {
  if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Config filenames are required"
    exit 1
  fi

  local from=$1
  local to="$2"
  if [ -f "$to" ]; then
    rm $to
  fi

  local ENV=$3
  echo "# ALOES API CONFIG
NODE_ENV=$(read_var NODE_ENV $from)
APP_NAME=$(read_var APP_NAME $from)
ADMIN_EMAIL=$(read_var ADMIN_EMAIL $from)
HTTP_SERVER_PORT=$(read_var HTTP_SERVER_PORT $from)
REST_API_ROOT=$(read_var REST_API_ROOT $from)
COOKIE_SECRET=
WS_BROKER_PORT=$(read_var WS_BROKER_PORT $from)
WSS_BROKER_PORT=$(read_var WSS_BROKER_PORT $from)
MQTT_BROKER_PORT=$(read_var MQTT_BROKER_PORT $from)
MQTTS_BROKER_PORT=$(read_var MQTTS_BROKER_PORT $from)
# PROXY CONFIG
PROXY_SERVER_HOST=$(read_var PROXY_SERVER_HOST $from)
PROXY_DOMAIN=$(read_var PROXY_DOMAIN $from)
PROXY_EXTENSION=$(read_var PROXY_EXTENSION $from)
PROXY_IP=$(read_var PROXY_IP $from)
PROXY_HTTP_SERVER_PORT=$(read_var PROXY_HTTP_SERVER_PORT $from)
PROXY_HTTPS_SERVER_PORT=$(read_var PROXY_HTTPS_SERVER_PORT $from)
PROXY_MQTT_BROKER_PORT=$(read_var PROXY_MQTT_BROKER_PORT $from)
PROXY_MQTTS_BROKER_PORT=$(read_var PROXY_MQTTS_BROKER_PORT $from)" >> "$to"
}

create_env() {
  if [ -z "$1" ]; then
    echo "Environment name is required to build services"
    exit 1
  fi

  local ENV=$1
  local config="$(pwd)/.env"
  local config_tmp="$(pwd)/.env.tmp"
  local from_env=0
  # config="$(pwd)/.env.$ENV"

  if [ -f "$config_tmp" ]; then
    rm $config_tmp
  fi

  if [ -f "$config" ]; then
    read -p "Config file exists, continue and override ? (y/N) " answer
    if [ "$answer" == "Y" ] || [ "$answer" == "y" ]; then
      read -p "Use current configuration as a template ? (y/N) " answer
      if [ "$answer" == "Y" ] || [ "$answer" == "y" ]; then
        cp $config $config_tmp
      fi
    else 
      exit
    fi
  fi
  local env_keys

  read -p "Would you like to configure Aloes API ? (y/N) " answer
  if [ "$answer" == "Y" ] || [ "$answer" == "y" ]; then
    env_keys=(NODE_ENV APP_NAME HTTP_SERVER_PORT MQTT_BROKER_PORT MQTTS_BROKER_PORT WS_BROKER_PORT 
      WSS_BROKER_PORT REST_API_ROOT ADMIN_EMAIL)
    for env_key in "${env_keys[@]}"; do
      get_value_from_user $env_key $config_tmp $from_env
      set_env $env_key $USER_VAL $config_tmp
    done
  fi

  # todo set default PROXY_IP based on machine ip ?
  read -p "Would you like to configure Nginx proxy ? (y/N) " answer
  if [ "$answer" == "Y" ] || [ "$answer" == "y" ]; then
    env_keys=(PROXY_SERVER_HOST PROXY_HTTP_SERVER_PORT PROXY_MQTT_BROKER_PORT 
      PROXY_IP PROXY_DOMAIN PROXY_EXTENSION)
    for env_key in "${env_keys[@]}"; do
      get_value_from_user $env_key $config_tmp $from_env
      set_env $env_key $USER_VAL $config_tmp
    done

    if [ "$ENV" == "staging" ] || [ "$ENV" == "production" ]  ; then
      echo "Now configure Nginx proxy ssl"
      env_keys=(PROXY_HTTPS_SERVER_PORT PROXY_MQTTS_BROKER_PORT)
      for env_key in "${env_keys[@]}"; do
        get_value_from_user $env_key $config_tmp $from_env
        set_env $env_key $USER_VAL $config_tmp
      done
    fi
  fi

  replicate_env $config_tmp $config $ENV
  echo "Configuration saved in $config"
  rm $config_tmp
  
}

init_project () {
  # todo add a function to create A and AAAA DNS redirection ?
  if [ -z "$1" ]; then
    echo "Environment variable name is required to init project"
    exit 1
  fi

  local ENV=$1
  local env_file="$(pwd)/.env"

  local a=$SECONDS
  build_proxy local $env_file &
  # build_proxy $ENV $env_file &
  process_id=$!
  wait $process_id
  elapsedseconds=$(( SECONDS - a ))
  echo "Built proxy with status $? in $elapsedseconds s"
  if [ $? == 1 ]; then exit 1 ; fi

  a=$SECONDS
  local DOMAIN=$(read_var PROXY_SERVER_HOST $env_file)
  local ADMIN_EMAIL=$(read_var ADMIN_EMAIL $env_file)
  local data_path="$(pwd)/config/certbot"
  if [ -d "$data_path" ]; then
    read -p "Existing data found for $DOMAIN. Continue and replace existing certificate? (y/N) " answer
    if [ "$answer" != "Y" ] && [ "$answer" != "y" ]; then
      exit
    fi
  fi
  
  init_ssl $DOMAIN $ADMIN_EMAIL $ENV &
  # init_ssl "$DOMAIN www.$DOMAIN" $ADMIN_EMAIL &
  process_id=$!
  wait $process_id
  elapsedseconds=$(( SECONDS - a ))
  echo "Created ssl certificates with status $? in $elapsedseconds s"
  if [ "$?" != "0" ]; then exit 1 ; fi

}

create() {
  if [ -z "$1" ]; then
    echo "Environment variable name is required to create project"
    exit 1
  fi

  local ENV=$1

  echo "Welcome to Aloes creation helper, you will start by creating your .env file.
When prompted type your answer, followed by [ENTER]"
  sleep 1
  create_env $ENV
  # process_id=$!
  # wait $process_id
  read -p "Would you like to generate ssl certificates now ? (y/N) " answer
  if [ "$answer" != "Y" ] && [ "$answer" != "y" ]; then
    exit
  else 
    init_project $ENV
  fi

  # if [ $? -ne 0 ]; then
  #   echo "Aloes environment creation cancelled"
  # fi
}