# Aloes - Nginx docker

Quickly configure and start Nginx to proxy Aloes API ( WS, HTTP and MQTT ).
All commands must be executed from the root project directory.

### Setup :

`./config/nginx-gw-*.template` are loaded by the dockerfile based on environment or build-args and can be updated to suit your needs. Should work out of the box.

`.env` must be created using `.env.sample` as a template, to reflect your Aloes config.
You can also use `./aloes.sh` helper by passing arguments to the CLI ( or not ) and open GUI assistant.  

```bash
  $ ./aloes.sh -c create
```

### Build

```bash
  $  docker build --no-cache -t nginx-gw -f config/nginx/gw-dockerfile ./config/nginx

  $  docker build --no-cache -t nginx-gw -f config/nginx/gw-dockerfile ./config/nginx --build-arg nginx_template=aloes-gw.template

  $  docker build --no-cache -t aloes-gw-stage -f config/nginx/gw-dockerfile ./config/nginx --build-arg nginx_template=aloes-gw-staging.template --build-arg proxy_server_host=HOST

  $ ./aloes.sh -c build -e staging
```

### Start

```bash
  $  docker run --rm -it --name nginx-gw -v "$(pwd)"/log/nginx:/var/log/nginx --net="host" nginx-gw

  $  docker run --rm -it --name nginx-gw --net="host" nginx-gw

  $  docker run -dit --restart unless-stopped --name nginx-gw --net="host" nginx-gw

  $  docker run -it --rm --name aloes-gw-stage -v "$(pwd)"/config/openssl:/etc/nginx/certs --net="host" aloes-gw-stage

  $ ./aloes.sh -c start -e staging
```
### Stop

```bash
  $  docker stop nginx-gw

  $ ./aloes.sh -c stop -e staging
```

### Remove

```bash
  $  docker container rm nginx-gw
```

### Log

```bash
  $  docker logs nginx-gw --follow --tail="100"

  $ ./aloes.sh -c log -e staging
```

## To deploy with your own TLS / SSL certificates

Create a folder with Hostname in `./config/openssl`, and copy/generate certificates and keys in it.

```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./config/openssl/<HOST>/key.pem -out ./config/openssl/<HOST>/cert.pem

openssl dhparam -out ./config/openssl/<HOST>/dhparam.pem 2048
```
